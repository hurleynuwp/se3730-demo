#include "gtest/gtest.h"
#include "SquareRoot.h"
#include <tgmath.h> 

TEST(ValidTests, Valid)
{
	SquareRoot sq = SquareRoot();
	
	double result = sq.GetSquareRoot(5);
	ASSERT_EQ(result, sqrt(5));

	result = sq.GetSquareRoot(10);
	ASSERT_EQ(result, sqrt(10));

	result = sq.GetSquareRoot(25);
	ASSERT_EQ(result, sqrt(25));
}

TEST(InvalidTests, Invalid)
{
	SquareRoot sq = SquareRoot();

	double result = sq.GetSquareRoot(5);
	ASSERT_NE(result, sqrt(25));

	result = sq.GetSquareRoot(10);
	ASSERT_NE(result, sqrt(100));

	result = sq.GetSquareRoot(25);
	ASSERT_NE(result, sqrt(5));
}

int main(int argc, char** argv)
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

#include <iostream>
#include "SquareRoot.h"

int main(int argc, char** argv)
{
    SquareRoot sq;
    double sqrt = sq.GetSquareRoot(argc);
    std::cout << "The square root of " << argc << " = " << sqrt << std::endl;
    return 0;
}

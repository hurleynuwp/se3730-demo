#include "SquareRoot.h"
#include <valarray> 

double SquareRoot::GetSquareRoot(int input)
{
    double sqroot = 1;
    sqroot = sqrt(input);
    return sqroot;
}
